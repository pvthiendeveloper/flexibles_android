package team.kiti.flexibles.extensions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics

inline fun <reified T: Any> newIntent(context: Context): Intent{
    return Intent(context, T::class.java)
}

// Start activity
inline fun <reified T: Any> Context.launchActivity(
    options: Bundle? = null,
    noinline init: Intent.() -> Unit = {}
){
    val intent = newIntent<T>(this)
    intent.init()
    this.startActivity(intent, options)
}

inline fun <reified T: Any> Activity.launchActivity(
    options: Bundle? = null,
    noinline init: Intent.() -> Unit = {}
){
    val intent = newIntent<T>(this)
    intent.init()
    this.startActivity(intent, options)
}

inline fun <reified T: Any> Activity.launchActivityForResult(
    requestCode: Int = -1,
    options: Bundle? = null,
    noinline init: Intent.() -> Unit = {}
){
    val intent = newIntent<T>(this)
    intent.init()
    this.startActivityForResult(intent, requestCode, options)
}

fun Activity.width(): Int {
    val displayMetrics = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.widthPixels
}

fun Activity.height(): Int {
    val displayMetrics = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.heightPixels
}