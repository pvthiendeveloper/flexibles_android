package team.kiti.flexibles.util

import android.content.Context
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class GridHoriManager(
    context: Context,
    count: Int = 1
) : GridLayoutManager(context, count, RecyclerView.HORIZONTAL, false)