package team.kiti.flexibles.widgets

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView

class ResizeImageView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {

    private fun resize(width: Int, height: Int) {
        layoutParams.width = width
        layoutParams.height = height
        requestLayout()
    }

    private fun scaleWidth(width: Int, height: Int, newWidth: Int) {
        val h = (newWidth * height) / width
        resize(newWidth, h)
    }

    private fun scaleHeight(width: Int, height: Int, newHeight: Int) {
        val w = (newHeight * width) / height
        resize(w, newHeight)
    }

    fun scalePriorityWithNewSize(width: Int, height: Int, newWidth: Int, newHeight: Int) {
        if (width > height) {
            scaleWidth(width, height, newWidth)
        } else {
            scaleHeight(width, height, newHeight)
        }
    }
}