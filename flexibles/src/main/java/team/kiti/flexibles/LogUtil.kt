package team.kiti.flexibles

import android.util.Log

fun logd(tag: String? = null, msg: String) {
    var myTag = tag
    if (myTag.isNullOrEmpty()) {
        myTag = "[GPLX_DEBUG]"
    }
    Log.d(myTag, msg)
}